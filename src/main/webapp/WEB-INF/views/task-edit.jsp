<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<h1>TASK EDIT</h1>
<form:form action="/task/edit/${task.id}/" modelAttribute="task" method="post">
    <input type="hidden" name="id" value="${task.id}">

    <p>
    <div>NAME:</div>
    <div><input type="text" name="name" value="${task.name}"/></div>
    <form:errors path="name"/>
    </p>

    <p>
    <div>DESCRIPTION:</div>
    <div><input type="text" name="description" value="${task.description}"/></div>
    <form:errors path="description"/>
    </p>

    <p>

        PROJECT (necessarily): <form:select path="project.id">
        <form:option value="select project for current task.."/>
        <c:forEach var="currentProject" items="${projectList}">

            <form:option value="${currentProject.id}"/>

        </c:forEach>
    </form:select>
    </p>

    <button type="submit">SAVE TASK</button>

</form:form>
<jsp:include page="../include/_footer.jsp"/>