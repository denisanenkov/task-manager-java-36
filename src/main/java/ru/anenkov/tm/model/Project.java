package ru.anenkov.tm.model;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
public class Project {

    @Id
    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
    private List<Task> tasks;

    public Project(String name) {
        this.name = name;
    }

    public Project() {
    }

    @Override
    public String toString() {
        return "Project{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
