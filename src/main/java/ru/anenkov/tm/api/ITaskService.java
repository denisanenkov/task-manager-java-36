package ru.anenkov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task findById(@Nullable final String id);

    void save(@Nullable final Task task);

    void deleteById(@Nullable final String id);

}
