package ru.anenkov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project findById(@Nullable final String id);

    void save(@Nullable final Project project);

    void deleteById(@Nullable final String id);

}
