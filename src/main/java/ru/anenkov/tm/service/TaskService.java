package ru.anenkov.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.model.Task;
import ru.anenkov.tm.repository.TaskRepository;

import java.util.List;

@Service
public class TaskService {

    @Nullable
    @Autowired
    TaskRepository taskRepository;

    @Nullable
    @Transactional(readOnly = true)
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Nullable
    @Transactional(readOnly = true)
    public Task findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(id).orElse(null);
    }

    @Transactional
    public void save(@Nullable final Task task) {
        if (task == null) throw new EmptyEntityException();
        taskRepository.save(task);
    }

    @Transactional
    public void deleteById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteById(id);
    }

}
