package ru.anenkov.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.IProjectService;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.model.Project;

import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @Nullable
    @Autowired
    ProjectService projectService;

    @Nullable
    @Transactional(readOnly = true)
    public List<Project> findAll() {
        return projectService.findAll();
    }

    @Nullable
    @Transactional(readOnly = true)
    public Project findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectService.findById(id);
    }

    @Transactional
    public void save(@Nullable final Project project) {
        if (project == null) throw new EmptyEntityException();
        projectService.save(project);
    }

    @Transactional
    public void deleteById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectService.deleteById(id);
    }

}
