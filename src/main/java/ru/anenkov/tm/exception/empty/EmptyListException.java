package ru.anenkov.tm.exception.empty;

public class EmptyListException extends ArithmeticException {

    public EmptyListException() {
        System.out.println("[LIST OF ENTITIES IS EMPTY]");
    }

}
