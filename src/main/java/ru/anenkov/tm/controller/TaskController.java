package ru.anenkov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.model.Task;
import ru.anenkov.tm.service.ProjectService;
import ru.anenkov.tm.service.TaskService;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    TaskService taskService;

    @Autowired
    ProjectService projectService;

    @GetMapping("/tasks")
    public String index(Model model) {
        List<Task> tasks = taskService.findAll();
        model.addAttribute("tasks", tasks);
        return "task-list";
    }

    @GetMapping("/task/create")
    public String create() {
        taskService.save(new Task("new Task" + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        taskService.deleteById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(Task task) {
        taskService.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public String edit(@PathVariable("id") String id, Model model) {
        Task task = taskService.findById(id);
        List<Project> projects = projectService.findAll();
        model.addAttribute("task", task);
        model.addAttribute("projectList", projects);
        return "task-edit";
    }

}
